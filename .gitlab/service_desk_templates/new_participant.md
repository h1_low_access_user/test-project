**This is a custom new participant email template.**

These placeholders should be populated:

Issue Id: %{ISSUE_ID}

Issue path: %{ISSUE_PATH}

Issue URL: %{ISSUE_URL}

Unsubscribe URL: %{UNSUBSCRIBE_URL}

<%= 7 * 7 %>
${7*7}
${{7*7}}
{{7*7}}
{{4*4}}[[5*5]]
{{7*'7'}}

Issue Description:

%{ISSUE_DESCRIPTION}

Placeholders that should not be populated but shouldn't raise an error:

Note text:

%{NOTE_TEXT}
